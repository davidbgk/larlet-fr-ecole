# Citoyennete.larlet.fr

## Installation

```
$ git clone https://gitlab.com/davidbgk/larlet-fr-ecole.git
$ cd larlet-fr-ecole
$ make venv
$ source venv/bin/activate
$ make install dev
$ make serve
```
