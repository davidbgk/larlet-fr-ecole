# Accueil

J’en avais marre de ne pas avoir les documents au moment de faire les devoirs :sweat_smile:.

!!! tip "Site de Carolyne"

    Le [site de la classe par Carolyne](https://sites.google.com/site/carolyne5bfernandseguin/) qui contient notamment [les leçons](https://sites.google.com/site/carolyne5bfernandseguin/devoirs-et-le%C3%A7ons).

## Documents officiels ([source](https://fernand-seguin.cssdm.gouv.qc.ca/liens-formulaires/))

### Horaires

??? info "Journée d’école de 8h34/40 à 15h46/49. Dîner de 11h50 à 13h05. Bibli de 13h08 à 14h02."

    ![](media/Horaire-FS-primaire-2024-2025.png)

    [Horaires FS 2024-2025](media/Horaire-FS-primaire-2024-2025.pdf) (pdf, 123Ko)

### Calendrier

![](media/Calendrier-FS-2024-10-12.png)
![](media/Calendrier-FS-2025-01-03.png)
![](media/Calendrier-FS-2024-legende.png)

[Calendrier scolaire FS 2024-2025](media/Calendrier-FS-2024-25_V.20241205.pdf) (pdf)
