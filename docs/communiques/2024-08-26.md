# Communiqué aux parents du 26 aout 2024

Bonjour chers parents,
J'espère que vous avez bien profité des vacances estivales ! Je vous souhaite une excellente rentrée scolaire ainsi qu'à votre jeune! Votre enfant du niveau primaire recevra un accueil chaleureux de ses enseignants.e le mardi 27 aout prochain!
Pour le préscolaire, ils sont attendus dans la petite cour des maternelles à l'heure indiquée dans la lettre qui vous a été envoyée le 5 juillet dernier. Les élèves du primaire sont attendus dans la cour d'école à 8 h 34 pour le lancement de l'année
2024-2025, « Découvrir le monde! »
Les listes des groupes seront affichées sur la clôture parallèle à la rue Millen. Un drapeau d'un pays permettra de jumeler votre enfant à son titulaire
Exceptionnellement pour cette journée spéciale, nous permettrons aux parents des élèves du primaire d'entrer dans la cour arrière. Nous présenterons les membres du personnel enseignants et votre enfant devra par la suite rejoindre son enseignant ou son enseignante titulaire pour l'année!
Veuillez prendre connaissance des informations contenues dans ce communiqué.
## Documents pour la rentrée 2024
Les documents de la rentrée 2024 sont disponibles sur le site Web de l'école en suivant le lien suivant : https://fernand-seguin.cssdm.gouv.qc.ca/liens-formulaires/ Vous y trouverez le calendrier (p.j.), les horaires préscolaire et primaire (p.j.), les listes de matériel par niveaux, les listes des frais exigés aux parents par niveau, etc. Vous aurez accès à l'état de compte à payer pour les frais exigés aux parents via Mozaïk et un message sera envoyé pour vous en informer. Veuillez noter qu'un délai, de 3 à 5 jours ouvrables, est parfois nécessaire avant que votre institution financière ne traite la transaction. Veuillez nous excuser si vous recevez l'état de compte suite au paiement. Cet envoi cessera automatiquement lorsque le paiement sera enregistré dans le compte de l'école et que votre solde sera à zéro.
## Travaux aux abords de l'école
La Ville de Montréal procède à des travaux de réhabilitation des conduites d'eau aux abords de l'école. Nous vous demandons de respecter la signalisation, les limites de vitesses et les espaces de débarcadères. Vous devez traverser la rue aux intersections; vous êtes des modèles pour votre jeune! La sécurité des élèves demeure une préoccupation commune et vous pouvez nous faire part de vos observations. Le chargé de projet m'a confirmé que les employés sont à terminer le pavage et le balayage en prévision de la rentrée de demain! Nous vous recommandons de prévoir passer par la rue Millen puisque l'avenue Durham et la rue Sauriol ne seront peut être pas fluides à la circulation.
## Rencontre parents-enseignants et AGA du 11 septembre 2024
Vous êtes invités à rencontrer l'enseignant.e de votre enfant le mercredi 11 septembre 2024 pour une rencontre de 18 h 30 à 19 h 15, dans le local de classe de votre enfant. C'est l'occasion de prendre connaissance du contenu des cours, des exigences concernant les travaux et évaluations qui auront cours cette année.
Cette rencontre sera suivie par l'assemblée générale annuelle des parents (AGA) qui aura lieu à 19 h 30. Les élections pour les postes à pourvoir auront lieu à ce moment. Il y aura possibilité de former un comité pour l'organisme de participation des parents (OPP), suivi par l'AGA de la Fondation Fernand-Seguin de Montréal.
## Photo scolaire 2024
Le photographe qui a été retenu cette année est EnfantsClik. La photo aura lieu le 23 octobre 2024.
## Attestation de fréquentation scolaire du 30 septembre
Il est important que votre enfant soit présent à l'école pour la déclaration de clientèle du 30 septembre. Cette opération se déroule dans toutes les écoles du Québec et ce processus permet l'octroi du financement des écoles. Veuillez prendre cela en considération lorsque vous prenez des rendez-vous pour vos enfants.

## Activités extrascolaires LSB et activités parascolaires
En suivant ce lien (https://www.loisirssophiebarat.ca/quand-comment-sinscrire/), vous trouverez très bientôt un dépliant de Loisirs Sophie-Barat (LSB), organisme de l'arrondissement Ahuntsic-Cartierville permettant d'inscrire vos enfants à des activités après les heures de classe et du SDG. Veuillez noter que l'école ne gère pas ces activités ni les inscriptions.
Cordialement,
Mate Dug
Martin Dugas, directeur

