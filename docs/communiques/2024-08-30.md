# Communiqué aux parents du 30 aout 2024

Bonjour chers parents,
Veuillez prendre connaissance des informations contenues dans ce communiqué.
Message du service de garde de l'école
Chers parents,
J'espère que la rentrée s'est faite en douceur et que vos enfants sont heureux de retrouver leurs amis.
## Je vous fais parvenir aujourd'hui le lien du Padlet du service de garde de l'école.
https://padlet.com/berniern2/sdg-fernand-seguin-lpzh4urni3k9q3pg
Vous y trouverez toutes les informations nécessaires à la vie au service de garde.
J'y ai aussi ajouté une colonne parascolaire, bien lire les informations, certaines inscriptions sont déjà ouvertes d'autres ouvriront bientôt!
Prendre note que je ne gère pas les activités parascolaires, vous devez contacter les organismes si vous avez des questions.
Nous avons aussi un nouveau traiteur, Le Doral. Les coordonnées sont dans la section: La boite à lunch.
A noter que tous les départs des enfants qui sont inscrits à temps plein au service de garde se font par la porte B du service de garde située sur la rue
Sauriol.
Cangara mason ex lesionnelte eisont inscrits pour le diner, mais qui iraient
SVP pour toutes questions ou informations, utiliser la boite courriel:
fseguin.sdg@cssdm.gouv.qc.ca
Bonne année scolaire à tous!
Cordialement, Nancy Bernier Technicienne
## Communications de la part des membres du personnel
Il a été porté à mon attention que les courriels envoyés par les membres du personnel sont parfois dirigés vers votre boîte de pourriels ou indésirables par votre service de messagerie. Veuillez vérifier et répondre aux messages envoyés par les enseignants.es. Merci!
## Portes ouvertes 2024 des écoles secondaires du CSSDM
Alors que la rentrée scolaire est tout juste derrière nous, nos écoles secondaires s'activent déjà pour ouvrir leurs portes afin d'accueillir parents et élèves. Ce rendez-vous annuel est l'occasion pour les parents et les élèves de 5e et 6e années du primaire de découvrir les options variées et les programmes stimulants offerts par nos établissements. Les portes ouvertes de 24 écoles secondaires du CSSDM seront accessibles entre le 16 septembre et le 27 octobre 2024
(Voir les horaires : https://a-ma-portee.cssdm.gouv.qc.ca/portes-ouvertes).
## Documents à retourner au titulaire de votre enfant
Si cela n'a pas encore été fait, il est important de retourner les documents suivants au titulaire de votre enfant le plus rapidement possible.
- Sorties dans le quartier 2024-2025;
- Autorisation d'être photographié;
- Parent bénévole ET Antécédents judiciaires (si jamais fait).
Les documents se trouvent sur le site Internet de l'école :
https://fernand-seguin.cssdm.gouv.qc.ca/liens-formulaires/
## Facture à payer (Rappel)
La facture à payer pour les frais scolaires est accessible via votre Mozaïk Portail.
Veuillez acquitter la somme au plus tard le 30 septembre 2024. Merci!
Je vous souhaite une belle fin de semaine de la fête du travail!
Cordialement,
Mater Dug
Martin Dugas, directeur

