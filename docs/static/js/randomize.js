document.addEventListener('DOMContentLoaded', () => {
  document.querySelectorAll('.md-content__inner ol').forEach((ol) => {
    const button = document.createElement('button')
    button.textContent = 'Mélanger'
    button.type = 'button'
    ol.insertAdjacentElement('beforebegin', button)
    button.addEventListener('click', (ev) => {
      for (let i = ol.children.length; i >= 0; i--) {
        ol.appendChild(ol.children[(Math.random() * i) | 0])
      }
    })
  })
})
