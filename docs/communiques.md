# Communiqués

Une extraction automatisée, c’est pas beau mais ça permet au moins de faire une recherche dedans :sweat_smile:.

## [2024-09-06](communiques/2024-09-06.md) ([pdf](communiques/2024-09-06.pdf))
## [2024-08-30](communiques/2024-08-30.md) ([pdf](communiques/2024-08-30.pdf))
## [2024-08-29](communiques/2024-08-29.md) ([pdf](communiques/2024-08-29.pdf))
## [2024-08-26](communiques/2024-08-26.md) ([pdf](communiques/2024-08-26.pdf))
## [2024-08-15](communiques/2024-08-15.md) ([pdf](communiques/2024-08-15.pdf))
